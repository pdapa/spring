/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developpez.rpouiller.tutoriel.web.spring.dao;

/**
 *
 * @author Patrick DAPA
 */
import com.developpez.rpouiller.tutoriel.web.spring.bean.Course;
import java.util.List;

public interface IServiceListeCourses {

    List<Course> rechercherCourses();

    void creerCourse(final String pLibelle, final Integer pQuantite);

    void supprimerCourse(final Integer pIdCourse);

    void modifierCourses(final List<Course> pListeCourses);
}
