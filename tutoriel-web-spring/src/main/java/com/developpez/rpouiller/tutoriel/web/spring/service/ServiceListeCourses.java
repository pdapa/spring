/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developpez.rpouiller.tutoriel.web.spring.service;

/**
 *
 * @author Patrick DAPA
 */
import com.developpez.rpouiller.tutoriel.web.spring.bean.Course;
import com.developpez.rpouiller.tutoriel.web.spring.dao.IListeCoursesDAO;
import com.developpez.rpouiller.tutoriel.web.spring.dao.IServiceListeCourses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServiceListeCourses implements IServiceListeCourses {

    @Autowired
    private IListeCoursesDAO dao;

    @Transactional(readOnly = true)
    @Override
    public List<Course> rechercherCourses() {
        return dao.rechercherCourses();
    }

    /**
     *
     * @param pLibelle
     * @param pQuantite
     */
    @Transactional
    @Override
    public void creerCourse(final String pLibelle, final Integer pQuantite) {
        final Course lCourse = new Course();
        lCourse.setLibelle(pLibelle);
        lCourse.setQuantite(pQuantite);

        dao.creerCourse(lCourse);
    }

    @Transactional
    @Override
    public void supprimerCourse(final Integer pIdCourse) {
        final Course lCourse = new Course();
        lCourse.setId(pIdCourse);

        dao.supprimerCourse(lCourse);
    }

    @Transactional
    @Override
    public void modifierCourses(final List<Course> pListeCourses) {
        for (final Course lCourse : pListeCourses) {
            dao.modifierCourse(lCourse);
        }
    }
}
