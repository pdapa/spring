/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developpez.rpouiller.tutoriel.web.spring.controller;

/**
 *
 * @author Patrick DAPA
 */
import com.developpez.rpouiller.tutoriel.web.spring.bean.Course;
import com.developpez.rpouiller.tutoriel.web.spring.dao.IServiceListeCourses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SupprimerListeCoursesController {

    @Autowired
    private IServiceListeCourses service;

    @RequestMapping(value = "/afficherSuppressionListeCourses", method = RequestMethod.GET)
    public String afficher(final ModelMap pModel) {
        final List<Course> lListeCourses = service.rechercherCourses();
        pModel.addAttribute("listeCourses", lListeCourses);
        return "suppression";
    }

    @RequestMapping(value = "/supprimerSuppressionListeCourses", method = RequestMethod.GET)
    @SuppressWarnings("empty-statement")
    public String supprimer(@RequestParam(value = "idCourse") final Integer pIdCourse, final ModelMap pModel) {

        service.supprimerCourse(pIdCourse);;
        return afficher(pModel);
    }
}
